import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TrainService} from '../service/train.service';
import {Train} from '../model/Train';
import {MatTableDataSource} from '@angular/material/table';
import {FormBuilder} from '@angular/forms';
import {BookTrain} from '../model/BookTrain';

@Component({
  selector: 'app-book-add',
  templateUrl: './book-add.component.html',
  styleUrls: ['./book-add.component.css']
})
export class BookAddComponent implements OnInit {

  constructor(private trainService: TrainService,
              public route: ActivatedRoute,
              private router: Router,
              private fb: FormBuilder) {
  }

  bookForm = this.fb.group({
    numberOfPLaces: [''],
    sales: [''],
    train: [''],
  });
  book: BookTrain = { bookNumber: 0, numberOfPLaces: 0, sales: 0, train: null};

  ngOnInit(): void {
  }

  public submit(): void {
    const trainData = {
      numTrain : parseInt(this.bookForm.get('train').value, 10),
    };
    const data = {
      numberOfPLaces : parseInt(this.bookForm.get('numberOfPLaces').value, 10),
      sales : parseInt(this.bookForm.get('sales').value, 10),
      train : trainData,
    };
    this.trainService.createBook(data).subscribe( book => {
      if (book) {
        this.router.navigate(['/book/list']).then((e) => {
          if (!e) {
            console.log('Navigation has failed!');
          }
        });
      }
    });
  }
}
