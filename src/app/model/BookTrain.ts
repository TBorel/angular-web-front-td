import {Train} from './Train';

export interface BookTrain {
  bookNumber: number;
  numberOfPLaces: number;
  sales: number;
  train: Train;
}
