import {BookTrain} from './BookTrain';

export interface Train {
  numTrain: number;
  villeDepart: string;
  villeArrivee: string;
  heureDepart: number;
  bookTrain: BookTrain;
}
