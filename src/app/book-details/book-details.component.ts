import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TrainService} from '../service/train.service';
import {Train} from '../model/Train';
import {MatTableDataSource} from '@angular/material/table';
import {FormBuilder} from '@angular/forms';
import {BookTrain} from '../model/BookTrain';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit {


  constructor(private trainService: TrainService,
              public route: ActivatedRoute,
              private router: Router,
              private fb: FormBuilder) {
  }


  bookForm = this.fb.group({
    numberOfPLaces: 0,
    sales: 0,
    train: 0,
  });
  book: BookTrain;

  ngOnInit(): void {
    this.getBookTrain();
  }

  private getBookTrain(): void {
    const id = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    this.trainService.getBookTrain(id).subscribe(
      book => {
        this.book = book;
        this.bookForm.get('numberOfPLaces').setValue(this.book.numberOfPLaces);
        this.bookForm.get('sales').setValue(this.book.sales);
        this.bookForm.get('train').setValue(this.book.train.numTrain);
      }
    );
  }

  public submit(): void {
    const trainData = {
      numTrain : parseInt(this.bookForm.get('train').value, 10),
    };
    const data = {
      numberOfPLaces : parseInt(this.bookForm.get('numberOfPLaces').value, 10),
      sales : parseInt(this.bookForm.get('sales').value, 10),
      train : trainData,
    };
    this.trainService.updateBook(data, this.book.bookNumber).subscribe( book => {
      if (book) {
        this.router.navigate(['/book/list']).then((e) => {
          if (!e) {
            console.log('Navigation has failed!');
          }
        });
      }
    });
  }
}
