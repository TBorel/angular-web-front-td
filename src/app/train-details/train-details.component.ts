import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TrainService} from '../service/train.service';
import {Train} from '../model/Train';
import {MatTableDataSource} from '@angular/material/table';
import {FormBuilder} from '@angular/forms';
import {BookTrain} from '../model/BookTrain';


@Component({
  selector: 'app-train-details',
  templateUrl: './train-details.component.html',
  styleUrls: ['./train-details.component.css']
})
export class TrainDetailsComponent implements OnInit {

  constructor(private trainService: TrainService,
              public route: ActivatedRoute,
              private router: Router,
              private fb: FormBuilder) {
  }


  trainForm = this.fb.group({
    numTrain: [''], // Valeur de départ vide
    villeDepart: [''],
    villeArrivee: [''],
    heureDepart: [''],
  });
  train: Train;
  private book: BookTrain;

  ngOnInit(): void {
    this.getTrain();
  }

  private getTrain(): void {
    const id = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    this.trainService.getTrain(id).subscribe(
      train => {
        this.train = train;
        this.trainForm.get('villeDepart').setValue(this.train.villeDepart);
        this.trainForm.get('villeArrivee').setValue(this.train.villeArrivee);
        this.trainForm.get('heureDepart').setValue(this.train.heureDepart);
      }
    );
  }

  public submit(): void {
    this.train.villeDepart = this.trainForm.get('villeDepart').value;
    this.train.villeArrivee = this.trainForm.get('villeArrivee').value;
    this.train.heureDepart = this.trainForm.get('heureDepart').value;
    this.trainService.updateTrain(this.train).subscribe( train => {
      if (train) {
        this.router.navigate(['/']).then((e) => {
          if (!e) {
            console.log('Navigation has failed!');
          }
        });
      }
    });
  }
}
