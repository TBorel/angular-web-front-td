import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TrainService} from '../service/train.service';
import {Train} from '../model/Train';
import {MatTableDataSource} from '@angular/material/table';
import {FormBuilder} from '@angular/forms';
import {BookTrain} from '../model/BookTrain';

@Component({
  selector: 'app-train-add',
  templateUrl: './train-add.component.html',
  styleUrls: ['./train-add.component.css']
})
export class TrainAddComponent implements OnInit {

  constructor(private trainService: TrainService,
              public route: ActivatedRoute,
              private router: Router,
              private fb: FormBuilder) {
  }

  trainForm = this.fb.group({
    numTrain: [''], // Valeur de départ vide
    villeDepart: [''],
    villeArrivee: [''],
    heureDepart: [''],
  });
  train: Train = { numTrain : 0, villeArrivee: '', villeDepart: '', heureDepart: 0, bookTrain: null };
  private book: BookTrain;

  ngOnInit(): void {
  }

  public submit(): void {
    this.train.villeDepart = this.trainForm.get('villeDepart').value;
    this.train.villeArrivee = this.trainForm.get('villeArrivee').value;
    this.train.heureDepart = parseInt(this.trainForm.get('heureDepart').value, 10);
    this.train.bookTrain = {bookNumber: 0, numberOfPLaces: 0, sales : 0, train: null};
    this.trainService.createTrain(this.train).subscribe( train => {
      if (train) {
        this.router.navigate(['/']).then((e) => {
          if (!e) {
            console.log('Navigation has failed!');
          }
        });
      }
    });
  }
}
