import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {TrainService} from '../service/train.service';
import {Train} from '../model/Train';
import {Observable} from 'rxjs';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatButtonModule} from '@angular/material/button';
import {Router} from '@angular/router';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  private trains: Train[] = null;
  displayedColumns: string[] = ['numTrain', 'villeDepart', 'villeArrivee', 'heureDepart', 'bookTrain', 'actions'];
  dataSource = new MatTableDataSource<Train>([]);

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private service: TrainService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getTrains();
  }

  public edit(train: Train): void
  {
    this.router.navigate(['/train', train.numTrain]).then((e) => {
      if (!e) {
        console.log('Navigation has failed!');
      }
    });
  }

  public add(): void
  {
    this.router.navigate(['/train/add']).then((e) => {
      if (!e) {
        console.log('Navigation has failed!');
      }
    });
  }

  public getTrains(): void
  {
    this.service.getTrains().subscribe(trains =>
    {
      this.dataSource.data = trains;
      this.dataSource.paginator = this.paginator;
    });
  }

  public remove(train: Train): void {
    this.service.removeTrain(train.numTrain).subscribe( () => this.getTrains() );
  }
}
