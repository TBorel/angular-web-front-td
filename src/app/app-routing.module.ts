import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ListComponent} from './list/list.component';
import {BookListComponent} from './book-list/book-list.component';
import {TrainDetailsComponent} from './train-details/train-details.component';
import {TrainAddComponent} from './train-add/train-add.component';
import {BookDetailsComponent} from './book-details/book-details.component';
import {BookAddComponent} from './book-add/book-add.component';



const routes: Routes = [
  { path: '', redirectTo: 'train/list', pathMatch: 'full' },
  { path: 'train/list', component: ListComponent},
  { path: 'train/add', component: TrainAddComponent},
  { path: 'train/:id', component: TrainDetailsComponent},
  { path: 'book/list', component: BookListComponent},
  { path: 'book/add', component: BookAddComponent},
  { path: 'book/:id', component: BookDetailsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
