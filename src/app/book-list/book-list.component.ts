import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {TrainService} from '../service/train.service';
import {BookTrain} from '../model/BookTrain';
import {Train} from '../model/Train';
import {Observable} from 'rxjs';
import {MatSort} from '@angular/material/sort';
import {MatButtonModule} from '@angular/material/button';
import {Router} from '@angular/router';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  private books: BookTrain[] = null;
  displayedColumns: string[] = ['bookNumber', 'numberOfPLaces', 'sales', 'actions'];
  dataSource = new MatTableDataSource<BookTrain>([]);

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private service: TrainService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.getBooks();
  }

  public getBooks(): void
  {
    this.service.getBooks().subscribe(books =>
    {
      this.dataSource.data = books;
      this.dataSource.paginator = this.paginator;
    });
  }

  public edit(book: BookTrain): void
  {
    this.router.navigate(['/book', book.bookNumber]).then((e) => {
      if (!e) {
        console.log('Navigation has failed!');
      }
    });
  }

  public add(): void {
    this.router.navigate(['/book/add']).then((e) => {
      if (!e) {
        console.log('Navigation has failed!');
      }
    });
  }

  public remove(book: BookTrain): void {
    this.service.removeBook(book.bookNumber).subscribe(
      () => {
        this.getBooks();
      }
    );
  }
}
