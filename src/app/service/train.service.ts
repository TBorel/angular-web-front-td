import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Train} from '../model/Train';
import { LocalStorageService, SessionStorageService, LocalStorage, SessionStorage } from 'angular-web-storage';
import {BookTrain} from '../model/BookTrain';


@Injectable({
  providedIn: 'root'
})
export class TrainService {

  private KEY = 'apiAddress';
  private trainUrl = '';  // URL to web api
  private apiUrl = 'http://192.168.226.128:8888';  // URL to web api

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  constructor(
    private http: HttpClient,
    private local: LocalStorageService
  ) {
    this.trainUrl = this.local.get(this.KEY);

    if (this.trainUrl == null) {
      this.trainUrl = this.apiUrl;
      this.local.set(this.KEY, this.trainUrl);
    }
    console.log(this.trainUrl);
  }

  getTrains(): Observable<Train[]> {
    return this.http.get<Train[]>(`${this.trainUrl}/train`);
  }

  getTrain(id: number): Observable<Train>
  {
    return this.http.get<Train>(`${this.trainUrl}/train/${id}`);
  }

  private getDataFromTrain(train: Train): object
  {
    const data = {
      villeDepart : train.villeDepart,
      villeArrivee : train.villeArrivee,
      heureDepart : train.heureDepart,
    };
    return data;
  }

  updateTrain(train: Train): Observable<Train> {
    return this.http.put<Train>(`${this.trainUrl}/train/${train.numTrain}`, this.getDataFromTrain(train), this.httpOptions);
  }

  createTrain(train: Train): Observable<Train> {
    return this.http.post<Train>(`${this.trainUrl}/train`, this.getDataFromTrain(train), this.httpOptions);
  }

  removeTrain(id: number): Observable<{}> {
    return this.http.delete<Train[]>(`${this.trainUrl}/train/${id}`);
  }

  getBookTrain(id: number): Observable<BookTrain>
  {
    return this.http.get<BookTrain>(`${this.trainUrl}/book/${id}`);
  }

  getBooks(): Observable<BookTrain[]> {
    return this.http.get<BookTrain[]>(`${this.trainUrl}/book`);
  }

  updateBook(book: object, id: number): Observable<BookTrain> {
    return this.http.put<BookTrain>(`${this.trainUrl}/book/${id}`, book, this.httpOptions);
  }

  createBook(book: object): Observable<BookTrain> {
    return this.http.post<BookTrain>(`${this.trainUrl}/book`, book, this.httpOptions);
  }

  removeBook(id: number): Observable<{}> {
    return this.http.delete<BookTrain[]>(`${this.trainUrl}/book/${id}`);
  }

}
